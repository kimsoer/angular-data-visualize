import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Province, Provinces } from '../model/provinces';

@Injectable({
  providedIn: 'root'
})
export class RemoteService {

  constructor(private http: HttpClient) { }

  getDataList(){  
      return this.http.get<Provinces>('http://159.223.61.117:8080/province')
  }

  update(province: Province){
   return this.http.post('http://159.223.61.117:8080/province',{
      "vulId": province.Vul_ID,
      "storm": province.STORM,
      "drought" : province.DROUGHT,
      "composite": province.COMPOSITE,
      "flood":province.FLOOD
    })
  }
}


