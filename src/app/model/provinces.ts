export interface Province {
    Vul_ID: number;
    COMMUNE_ID: number;
    YEAR_ID: number;
    STORM: number;
    DROUGHT: number;
    COMPOSITE: number;
    FLOOD: number;
    DISTRICT_ID: number;
    COMMUNE_ENG_NAME: string;
    COMMUNE_KH_NAME: string;
    YEAR_NAME: number;
    PROVINCE_ID: number;
    DISTRICT_ENG_NAME: string;
    DISTRICT_KH_NAME: string;
    PROFINCE_ENG_NAME: string;
    PROFINCE_KH_NAME: string;
}

export interface Provinces {
    message: string;
    data: Province[];
}

